<?php

class PersonneManager {
	protected $db;

		public function __construct($db){
			$this->db = $db;
		}
		// Qu'il soit étudiant ou pas, ceci ne change pas
		public function add($pers){
	        $requete = $this->db->prepare(
							'INSERT INTO personne (per_nom, per_prenom, per_tel, per_mail, per_login, per_pwd, per_admin) VALUES
	                        (:nom, :prenom, :tel, :mail, :login, :pwd, :adm);');

	        $requete->bindValue(':nom',$pers->getPerNom());
			$requete->bindValue(':prenom',$pers->getPerPrenom());
			$requete->bindValue(':tel',$pers->getPerTel());
			$requete->bindValue(':mail',$pers->getPerMail());
	        $requete->bindValue(':login',$pers->getPerLogin());
			$requete->bindValue(':adm',0);

	        $pwd = $pers->getPerPwd();
	        $salt = "48@!alsd";
	        $pwd =  sha1($pwd).$salt;
	        $pwd =  sha1($pwd);
	        $requete->bindValue(':pwd',$pwd);

	        $requete->execute();

			$id =  $this->db->lastInsertId();
			$requete->closeCursor();

			return $id;
	    }

		public function getAllPersonnes(){
            $listePersonnes = array();

            $sql = 'SELECT per_num, per_nom, per_prenom, per_tel, per_mail, per_admin, per_login, per_pwd FROM personne ORDER BY 1';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($ville = $requete->fetch(PDO::FETCH_OBJ)){
                $listePersonnes[] = new Personne($ville);
            }

            $requete->closeCursor();
            return $listePersonnes;
		}

		// Obtenir le nom pour l'afficher
		public function getNomByID($id){
			$sql = 'SELECT per_nom FROM personne WHERE per_num=:id';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':id',$id,PDO::PARAM_INT);

            $requete->execute();

			$nom = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			return $nom->per_nom;
		}

		public function getIdByPseudo($pseudo){
			$sql = 'SELECT per_num FROM personne WHERE per_login=:login';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':login',$pseudo,PDO::PARAM_STR);

            $requete->execute();

			$nom = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			return $nom->per_num;
		}

		public function isEtudiant($id){
			$sql = 'SELECT per_num FROM etudiant WHERE per_num=:id';

			$requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':id',$id,PDO::PARAM_INT);

            $requete->execute();

			$nom = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			return $nom; //Renvoie true ou false
		}

		public function isAdmin($id){
			$sql = 'SELECT per_admin FROM personne WHERE per_num=:id';

			$requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':id',$id,PDO::PARAM_INT);

            $requete->execute();

			$nom = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			if ($nom->per_admin == 1) {
				return true;
			}

			return false;
		}

		public function getPersonneById($id){
			$sql = 'SELECT per_num, per_nom, per_prenom, per_tel, per_mail, per_admin, per_login, per_pwd FROM personne WHERE per_num=:id';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':id',$id,PDO::PARAM_INT);

            $requete->execute();

			$personne = $requete->fetch(PDO::FETCH_OBJ);

			$nouvPers = new Personne($personne);

			$requete->closeCursor();

			return $nouvPers;
		}

		public function update($id, $nom, $pre, $tel, $mail, $login, $pwd){
			$sql = 'UPDATE personne SET per_nom=:nom,
						per_prenom=:pre,
						per_tel=:tel,
						per_mail=:mail,
						per_login=:login,
						per_pwd=:pwd
						WHERE per_num=:id';

			$requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':id',$id,PDO::PARAM_INT);
			$requete->bindValue(':nom',$nom,PDO::PARAM_STR);
			$requete->bindValue(':pre',$pre,PDO::PARAM_STR);
			$requete->bindValue(':tel',$tel,PDO::PARAM_STR);
			$requete->bindValue(':mail',$mail,PDO::PARAM_STR);
			$requete->bindValue(':login',$login,PDO::PARAM_STR);

			$salt = "48@!alsd";
			$pwd =  sha1(sha1($pwd).$salt);
			$requete->bindValue(':pwd',$pwd,PDO::PARAM_STR);


			$requete->execute();

			$requete->closeCursor();


		}

		public function supprimerPersonne($id){
			$cm = new CitationManager($this->db);
			$citations = $cm->getCitationsParPersonne($id);

			// On supprime toutes les citations de la personne et leurs votes
			foreach ($citations as $key => $value) {
				$cm->supprimerCitation($value->getCitNum());
			}

			$sql = 'DELETE FROM vote
						WHERE per_num=:id;

					DELETE FROM citation
								WHERE per_num_valide=:id;

					DELETE FROM citation
								WHERE per_num_etu=:id;

					DELETE FROM salarie
						WHERE per_num=:id;

					DELETE FROM etudiant
						WHERE per_num=:id;

					DELETE FROM personne
						WHERE per_num=:id;
						';

            $requete = $this->db->prepare($sql);

			$requete->bindValue(':id', $id, PDO::PARAM_INT);

            $requete->execute();

            $requete->closeCursor();
		}



}

?>
