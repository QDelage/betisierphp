<?php

class MotManager {
    private $dbo;

    public function __construct($db){
        $this->db = $db;
    }

    // Retourne les mots interdits détectés dans la chaine de caractère,
    // ou false sinon
    public function analyseStr($str){
        $mots = Array();

        $sql = 'SELECT mot_interdit FROM mot
                    WHERE MATCH (mot_interdit)
                    AGAINST (:str)';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        // En lowerCase pour éviter les erreurs de maj
        $requete->bindValue(':str',strToLower($str),PDO::PARAM_STR);

        $requete->execute();

        while ($mot = $requete->fetch(PDO::FETCH_OBJ)){
            $mots[] = $mot;
        }

        $requete->closeCursor();

        return $mots;
    }


}

?>
