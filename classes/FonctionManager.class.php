<?php
class FonctionManager {
    private $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function getAllFonctions(){
        $listeFct = array();

        $sql = 'SELECT fon_num, fon_libelle FROM fonction';

        $requete = $this->db->prepare($sql);

        $requete->execute();


        while ($fct = $requete->fetch(PDO::FETCH_OBJ)){
            $listeFct[] = new Fonction($fct);
        }

        $requete->closeCursor();

        return $listeFct;

    }
}

?>
