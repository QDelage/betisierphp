<?php
class DepartementManager {
    private $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function getAllDepartements(){
        $listeDepartements = array();

        $sql = 'SELECT dep_nom, dep_num, vil_nom FROM departement d
            JOIN ville v ON d.vil_num = v.vil_num';

        $requete = $this->db->prepare($sql);

        $requete->execute();


        while ($dep = $requete->fetch(PDO::FETCH_OBJ)){
            $listeDepartements[] = new Departement($dep);
        }

        $requete->closeCursor();

        return $listeDepartements;

    }
}

?>
