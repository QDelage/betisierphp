<?php
class Salarie extends Personne {


// Attributs
private $sal_telprof;
private $fon_num;


public function __construct($valeurs = array()){
	if (!empty($valeurs)){
        parent::__construct($valeurs);
        $this->affecteEtu($valeurs);
    }
}
public function affecteEtu($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
							case 'sal_telprof': $this->setTelProf($valeur); break;
							case 'fon_num': $this->setFonNum($valeur); break;

                    }
			}
	}

    public function getTelProf(){
		return $this->sal_telprof;
	}
	public function setTelProf($num){
	    $this->sal_telprof=$num;
    }

    public function getFonNum(){
		return $this->fon_num;
	}
	public function setFonNum($num){
	    $this->fon_num=$num;
    }

}
