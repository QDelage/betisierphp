<?php

class CitationManager {
	private $dbo;

		public function __construct($db){
			$this->db = $db;
		}
        public function add($per_num, $per_num_etu, $lib, $date){
            $requete = $this->db->prepare(
						'INSERT INTO citation (per_num, per_num_etu, cit_libelle, cit_date)
						 VALUES (:per, :etu, :lib, :date);');

			$requete->bindValue(':per', $per_num, PDO::PARAM_INT);
			$requete->bindValue(':etu', $per_num_etu, PDO::PARAM_INT);
			$requete->bindValue(':lib', $lib, PDO::PARAM_STR);
			$requete->bindValue(':date',$date, PDO::PARAM_STR);

            $requete->execute();

			$requete->closeCursor();

        }

		// Citations à lister
		public function getAllCitationsValides(){
            $listeCitations = array();

            $sql = 'SELECT cit_num, per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo FROM citation
				WHERE cit_valide=1
				AND cit_date_valide IS NOT NULL';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($citation = $requete->fetch(PDO::FETCH_OBJ)){
                $listeCitations[] = new Citation($citation);
            }

            $requete->closeCursor();
            return $listeCitations;
		}

		// Citations à valider
		public function getAllCitationsEnAttente(){
            $listeCitations = array();

            $sql = 'SELECT cit_num, per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo FROM citation
				WHERE cit_valide=0
				AND cit_date_valide IS NULL';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($citation = $requete->fetch(PDO::FETCH_OBJ)){
                $listeCitations[] = new Citation($citation);
            }

            $requete->closeCursor();
            return $listeCitations;
		}

		public function getAllCitations(){
            $listeCitations = array();

            $sql = 'SELECT cit_num, per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo
						FROM citation';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($citation = $requete->fetch(PDO::FETCH_OBJ)){
                $listeCitations[] = new Citation($citation);
            }

            $requete->closeCursor();
            return $listeCitations;
		}

		// Valider une citation
		public function validerCitation($cit, $per){
			$sql = 'UPDATE citation SET
						per_num_valide=:per,
						cit_valide=1,
						cit_date_valide=:date
						WHERE cit_num=:cit';

            $requete = $this->db->prepare($sql);

			$requete->bindValue(':cit', $cit, PDO::PARAM_INT);
			$requete->bindValue(':per', $per, PDO::PARAM_INT);

			$date = date("Y-m-d");
			$requete->bindValue(':date', $date, PDO::PARAM_STR);


            $requete->execute();

            $requete->closeCursor();
		}

		public function supprimerCitation($cit){
			$sql = 'DELETE FROM vote
						WHERE cit_num=:cit;

					DELETE FROM citation
						WHERE cit_num=:cit;';

            $requete = $this->db->prepare($sql);

			$requete->bindValue(':cit', $cit, PDO::PARAM_INT);

            $requete->execute();

            $requete->closeCursor();
		}

		// Utile pour pouvoir supprimer la personne ensuite
		public function getCitationsParPersonne($id){
			$listeCitations = array();

            $sql = 'SELECT cit_num, per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo
						FROM citation
						WHERE per_num=:id';

            $requete = $this->db->prepare($sql);
			$requete->bindValue(':id', $id, PDO::PARAM_INT);
            $requete->execute();

            while ($citation = $requete->fetch(PDO::FETCH_OBJ)){
                $listeCitations[] = new Citation($citation);
            }

            $requete->closeCursor();
            return $listeCitations;
		}

		// Requete à la volée pour la recherche
		public function rechercherCitation($prof, $date, $note){
            $listeCitations = array();

            $sql = 'SELECT c.cit_num, c.per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo
						FROM citation c
						JOIN vote v ON v.cit_num=c.cit_num
						WHERE true ';

			// Requête à la volée
			if ('null' != $prof) {
				$sql .= 'AND c.per_num=:prof ';
			}
			if (null != $date) {
				$sql .= 'AND cit_date=:date ';
			}
			if (null != $note) {
				$sql .= ' GROUP BY c.cit_num, c.per_num, per_num_valide, per_num_etu, cit_libelle, cit_date, cit_valide, cit_date_valide, cit_date_depo

				HAVING AVG(vot_valeur)=:note ';
			}

            $requete = $this->db->prepare($sql);

			if ('null' != $prof) {
				$requete->bindValue(':prof', $prof, PDO::PARAM_INT);
			}
			if (null != $date) {
				$requete->bindValue(':date', $date, PDO::PARAM_STR);
			}
			if (null != $note) {
				$requete->bindValue(':note', $note, PDO::PARAM_INT);
			}

            $requete->execute();

            while ($citation = $requete->fetch(PDO::FETCH_OBJ)){
                $listeCitations[] = new Citation($citation);
            }

            $requete->closeCursor();
            return $listeCitations;
		}


}

?>
