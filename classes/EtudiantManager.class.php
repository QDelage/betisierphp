<?php

class EtudiantManager extends PersonneManager {

    public function add($etu){


        // On l'a ajouté, mais il nous faut récupérer son identifiant désormais
        $etu->setPerNum(parent::add($etu));

        $requete = $this->db->prepare(
						'INSERT INTO etudiant (per_num, dep_num, div_num) VALUES
                        (:per, :dep, :div);');


        $requete->bindValue(':per',$etu->getPerNum());
		$requete->bindValue(':dep',$etu->getDepNum());
		$requete->bindValue(':div',$etu->getDivNum());



        $retour=$requete->execute();

        $requete->closeCursor();

		return $retour;
    }

    public function getDepartementById($id){
        $sql = 'SELECT dep_nom FROM etudiant e JOIN departement d ON e.dep_num = d.dep_num  WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->dep_nom;
    }

    public function getVilleById($id){
        $sql = 'SELECT vil_nom FROM etudiant e JOIN departement d ON e.dep_num = d.dep_num JOIN ville v ON d.vil_num=v.vil_num WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->vil_nom;
    }

    public function updateEtudiant($id, $nom, $pre, $tel, $mail, $login, $pwd, $div, $dep){
        parent::update($id, $nom, $pre, $tel, $mail, $login, $pwd);

        $sql = 'UPDATE etudiant SET dep_num=:dep,
                    div_num=:div
                    WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);
        $requete->bindValue(':dep',$dep,PDO::PARAM_INT);
        $requete->bindValue(':div',$div,PDO::PARAM_INT);


        $requete->execute();

        $requete->closeCursor();

    }

    public function getDivNumById($id){
        $sql = 'SELECT div_num FROM etudiant WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->div_num;
    }


    public function getDepNumById($id){
        $sql = 'SELECT dep_num FROM etudiant WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->dep_num;
    }
}

?>
