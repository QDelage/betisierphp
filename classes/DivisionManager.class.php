<?php
class DivisionManager {
    private $db;

    public function __construct($db){
        $this->db = $db;
    }

    public function getAllDivisions(){
        $listeDivisions = array();

        $sql = 'SELECT DISTINCT div_num, div_nom FROM division ORDER BY 2 ASC';

        $requete = $this->db->prepare($sql);

        $requete->execute();


        while ($div = $requete->fetch(PDO::FETCH_OBJ)){
            $listeDivisions[] = $div;
        }

        $requete->closeCursor();

        return $listeDivisions;

    }
}

?>
