<?php
class Departement {
// Attributs
private $dep_nom;
private $dep_num;
private $vil_num;

public function __construct($valeurs = array()){
	if (!empty($valeurs))
			 $this->affecte($valeurs);
}
public function affecte($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
                        case 'dep_nom': $this->setDepNom($valeur); break;
						case 'dep_num': $this->setDepNum($valeur); break;
						case 'vil_nom': $this->setVilNom($valeur); break;
					}
			}
	}
public function getDepNom(){
        return $this->dep_nom;
    }
public function setDepNom($nom){
        $this->dep_nom = $nom;
    }
public function getDepNum() {
        return $this->dep_num;
    }
public function setDepNum($num){
        $this->dep_num=$num;
    }

public function getVilNom() {
        return $this->vil_nom;
    }
public function setVilNom($nom){
        $this->vil_nom=$nom;
    }


}
