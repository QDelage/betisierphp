<?php
class Citation {

// Attributs
private $cit_num;
private $per_num;
private $per_num_valide;
private $per_num_etu;
private $cit_libelle;
private $cit_date;
private $cit_valide;
private $cit_date_depo;
private $vote_cit_num;
private $voteper_num;

public function __construct($valeurs = array()){
	if (!empty($valeurs))
			 $this->affecte($valeurs);
}
public function affecte($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
							case 'cit_num': $this->setCitNum($valeur); break;
							case 'per_num': $this->setPerNum($valeur); break;
                            case 'per_num_valide': $this->setPerNumValide($valeur); break;
                            case 'per_num_etu': $this->setPerNumEtu($valeur); break;
                            case 'cit_libelle': $this->setCitLibelle($valeur); break;
                            case 'cit_date': $this->setCitDate($valeur); break;
                            case 'cit_valide': $this->setCitValide($valeur); break;
							case 'cit_date_valide': $this->setCitDateValide($valeur); break;
                            case 'cit_date_depo': $this->setDateDepo($valeur); break;
                            case 'vote_cit_num': $this->setVoteCitNum($valeur); break;
                            case 'vote_per_num': $this->setVotePerNum($valeur); break;

                    }
			}
	}

	public function getCitNum(){
		return $this->cit_num;
	}
	public function setCitNum($num){
	    $this->cit_num=$num;
    }


	public function getPerNum(){
		return $this->per_num;
	}
	public function setPerNum($num){
		$this->per_num=$num;
	}


	public function getPerNumValide(){
		return $this->per_num_valide;
	}
	public function setPerNumValide($num){
		$this->per_num_valide=$num;
	}


	public function getPerNumEtu(){
		return $this->per_num_etu;
	}
	public function setPerNumEtu($num){
		$this->per_num_etu=$num;
	}


	public function getCitLibelle(){
		return $this->cit_libelle;
	}
	public function setCitLibelle($libelle){
		$this->cit_libelle=$libelle;
	}


	public function getCitDate(){
		return $this->cit_date;
	}
	public function setCitDate($date){
		$this->cit_date=$date;
	}


	public function getCitValide(){
		return $this->cit_valide;
	}
	public function setCitValide($citValide){
		$this->cit_valide=$citValide;
	}


	public function getCitDateValide(){
		return $this->cit_date_depo;
	}
	public function setCitDateValide($citDateValide){
		$this->cit_date_valide=$citDateValide;
	}

	public function getDateDepo(){
		return $this->cit_date_depo;
	}
	public function setDateDepo($citDateDepo){
		$this->cit_date_depo=$citDateDepo;
	}


	public function getVoteCitNum(){
		return $this->vote_cit_num;
	}
	public function setVoteCitNum($vote_cit_num){
		$this->vote_cit_num=$vote_cit_num;
	}


	public function getVotePerNum(){
		return $this->voteper_num;
	}
	public function setVotePerNum($voteper_num){
		$this->voteper_num=$voteper_num;
	}
}
