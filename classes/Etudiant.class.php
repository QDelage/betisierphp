<?php
class Etudiant extends Personne {


// Attributs
private $dep_num;
private $div_num;


public function __construct($valeurs = array()){
	if (!empty($valeurs)){
        parent::__construct($valeurs);
        $this->affecteEtu($valeurs);
    }
}
public function affecteEtu($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
							case 'dep_num': $this->setDepNum($valeur); break;
							case 'div_num': $this->setDivNum($valeur); break;

                    }
			}
	}

    public function getDepNum(){
		return $this->dep_num;
	}
	public function setDepNum($num){
	    $this->dep_num=$num;
    }

    public function getDivNum(){
		return $this->div_num;
	}
	public function setDivNum($num){
	    $this->div_num=$num;
    }

}
