<?php

class SalarieManager extends PersonneManager {

    public function add($sal){


        // On l'a ajouté, mais il nous faut récupérer son identifiant désormais
        $sal->setPerNum(parent::add($sal));

        $requete = $this->db->prepare(
						'INSERT INTO salarie (per_num, sal_telprof, fon_num) VALUES
                        (:per, :tel, :fon);');


        $requete->bindValue(':per',$sal->getPerNum());
		$requete->bindValue(':tel',$sal->getTelProf());
		$requete->bindValue(':fon',$sal->getFonNum());

        $retour=$requete->execute();

        $requete->closeCursor();

		return $retour;
    }

    public function getTelProById($id){
        $sql = 'SELECT sal_telprof FROM personne p JOIN salarie s ON p.per_num = s.per_num WHERE p.per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->sal_telprof;
    }

    public function getFonctionById($id){
        $sql = 'SELECT fon_libelle FROM personne p JOIN salarie s ON p.per_num = s.per_num JOIN fonction f ON s.fon_num=f.fon_NUM WHERE p.per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->fon_libelle;
    }

    public function getFonctionNumById($id){
        $sql = 'SELECT f.fon_num FROM personne p JOIN salarie s ON p.per_num = s.per_num JOIN fonction f ON s.fon_num=f.fon_NUM WHERE p.per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);

        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne->fon_num;
    }

    public function getAllEnseignants(){
        $sql = 'SELECT per_nom, p.per_num FROM personne p
            JOIN salarie s ON s.per_num = p.per_num
            JOIN fonction f ON f.fon_num = s.fon_num
            WHERE f.fon_libelle = \'Enseignant\'';

        $requete = $this->db->prepare($sql);

        $requete->execute();

        $listeEnseignants = Array();

        while ($ens = $requete->fetch(PDO::FETCH_OBJ)){
            $listeEnseignants[] = new Personne($ens);
        }

        $requete->closeCursor();

        return $listeEnseignants;
    }

    public function updateSalarie($id, $nom, $pre, $tel, $mail, $login, $pwd, $fct, $telPro){
        parent::update($id, $nom, $pre, $tel, $mail, $login, $pwd);

        $sql = 'UPDATE salarie SET sal_telprof=:telProf,
                    fon_num=:fct
                    WHERE per_num=:id';

        $requete = $this->db->prepare($sql);

        // Requete préparée
        $requete->bindValue(':id',$id,PDO::PARAM_INT);
        $requete->bindValue(':telProf',$telPro,PDO::PARAM_STR);
        $requete->bindValue(':fct',$fct,PDO::PARAM_INT);


        $requete->execute();

        $requete->closeCursor();

    }
}

?>
