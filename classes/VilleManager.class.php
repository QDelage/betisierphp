<?php

class VilleManager {
	private $dbo;

		public function __construct($db){
			$this->db = $db;
		}

        public function ajouterVille($ville){
			if (!$this->existe($ville)) {
				$requete = $this->db->prepare(
							'INSERT INTO ville (vil_nom) VALUES (:nom);');

	            $requete->bindValue(':nom',$ville, PDO::PARAM_STR);

	            $requete->execute();

				$requete->closeCursor();

				return true;
			}else {
				return false;
			}


        }

		public function getAllVilles(){
            $listeVilles = array();

            $sql = 'SELECT vil_num, vil_nom FROM ville ORDER BY 2';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($ville = $requete->fetch(PDO::FETCH_OBJ)){
                $listeVilles[] = new Ville($ville);
            }

            $requete->closeCursor();
            return $listeVilles;
		}

		public function existe($ville){
			$sql = 'SELECT vil_num FROM ville WHERE vil_nom=:nom';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':nom',$ville,PDO::PARAM_STR);

            $requete->execute();

			$ville = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			return $ville;
		}

		public function aDesDepartements($ville){
			$sql = 'SELECT dep_num FROM departement d
			 	JOIN ville v ON d.vil_num=v.vil_num WHERE v.vil_num=:nom';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':nom',$ville,PDO::PARAM_INT);

            $requete->execute();

			$ville = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

			return $ville;
		}

		public function supprimerVille($id){
			$sql = 'DELETE FROM `ville` WHERE vil_num=:num';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':num',$id,PDO::PARAM_INT);

            $requete->execute();

			$requete->closeCursor();

		}

		public function modifierVille($id, $nom){
			$sql = 'UPDATE ville
						SET vil_nom=:nom
						WHERE vil_num=:num';

            $requete = $this->db->prepare($sql);

			// Requete préparée
			$requete->bindValue(':num',$id,PDO::PARAM_INT);
			$requete->bindValue(':nom',$nom,PDO::PARAM_STR);

            $requete->execute();

			$requete->closeCursor();

		}
}

?>
