<?php
class Vote {

// Attributs
private $cit_num;
private $per_num;
private $vot_valeur;


public function __construct($valeurs = array()){
	if (!empty($valeurs))
			 $this->affecte($valeurs);
}
public function affecte($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
							case 'cit_num': $this->setCitNum($valeur); break;
							case 'per_num': $this->setPerNum($valeur); break;
                            case 'vot_valeur': $this->setVotValeur($valeur); break;

                    }
			}
	}

	public function getCitNum(){
		return $this->cit_num;
	}
	public function setCitNum($num){
	    $this->cit_num=$num;
    }


	public function getPerNum(){
		return $this->per_num;
	}
	public function setPerNum($num){
		$this->per_num=$num;
	}


	public function getVotValeur(){
		return $this->vot_valeur;
	}
	public function setVotValeur($num){
		$this->vot_valeur=$num;
	}



}
