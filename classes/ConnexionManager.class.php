<?php
class ConnexionManager {
    private $dbo;

    public function __construct($db){
        $this->db = $db;
    }

    // Connecter un utilisateur
    public function verifConnexion($log, $pwd){
        $sql = 'SELECT per_login, per_pwd FROM personne WHERE per_login=:log AND per_pwd=:pwd';

        $requete = $this->db->prepare($sql);

        $salt = "48@!alsd";
        $pwd =  sha1($pwd).$salt;
        $pwd =  sha1($pwd);


        // Requete préparée
        $requete->bindValue(':log',$log,PDO::PARAM_STR);
        $requete->bindValue(':pwd',$pwd,PDO::PARAM_STR);


        $requete->execute();

        $personne = $requete->fetch(PDO::FETCH_OBJ);

        $requete->closeCursor();

        return $personne; // Retourne true ou false
    }
}

?>
