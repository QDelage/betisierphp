<?php

class VoteManager {
	private $db;

		public function __construct($db){
			$this->db = $db;
		}

		public function getAllVotes(){
            $listeCitations = array();

            $sql = 'SELECT cit_num, per_num, vot_valeur FROM vote';

            $requete = $this->db->prepare($sql);
            $requete->execute();

            while ($votes = $vote->fetch(PDO::FETCH_OBJ)){
                $listeVotes[] = new Vote($citation);
            }

            $requete->closeCursor();

            return $listeVotes;
		}

        public function getMoyenneByCitation($idCitation){
            $sql = 'SELECT AVG(vot_valeur) AS moy FROM vote WHERE cit_num=:id';

            $requete = $this->db->prepare($sql);

            // Requete préparée
            $requete->bindValue(':id',$idCitation,PDO::PARAM_INT);

            $requete->execute();

            $moy = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

            return $moy->moy;
        }

		public function noterCitation($note, $auteur, $citation){
			if (!$this->exist($auteur, $citation)) {
				$requete = $this->db->prepare(
							'INSERT INTO vote (cit_num, per_num, vot_valeur) VALUES
								(:cit, :per, :vote);');

			    $requete->bindValue(':cit',$citation);
				$requete->bindValue(':per',$auteur);
				$requete->bindValue(':vote',$note);


			    $res = $requete->execute();

				$requete->closeCursor();

				return $res;
			} else {
				return false;
			}

		}

		private function exist($auteur, $citation){
			$requete = $this->db->prepare(
						'SELECT vot_valeur FROM vote WHERE cit_num=:cit AND per_num=:per');

		    $requete->bindValue(':cit',$citation);
			$requete->bindValue(':per',$auteur);


		    $requete->execute();

			$res = $requete->fetch(PDO::FETCH_OBJ);

			$requete->closeCursor();

            return $res;
		}
}

?>
