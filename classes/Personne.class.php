<?php
class Personne {

    // Pour récuperer le nom de l'auteur, créer une méthode dans la classe Personne qui renvoie le nom avec l'id en argu

// Attributs
private $per_num;
private $per_nom;
private $per_prenom;
private $per_tel;
private $per_mail;
private $per_admin;
private $per_login;
private $per_pwd;


public function __construct($valeurs = array()){
	if (!empty($valeurs))
			 $this->affecte($valeurs);
}

public function affecte($donnees){
			foreach ($donnees as $attribut => $valeur){
					switch ($attribut){
							case 'per_num': $this->setPerNum($valeur); break;
							case 'per_nom': $this->setPerNom($valeur); break;
                            case 'per_prenom': $this->setPerPrenom($valeur); break;
                            case 'per_tel': $this->setPerTel($valeur); break;
                            case 'per_mail': $this->setPerMail($valeur); break;
                            case 'per_admin': $this->setPerAdmin($valeur); break;
                            case 'per_login': $this->setPerLogin($valeur); break;
                            case 'per_pwd': $this->setPerPwd($valeur); break;
                    }
			}
	}

	public function getPerNum(){
		return $this->per_num;
	}
	public function setPerNum($num){
	    $this->per_num=$num;
    }


	public function getPerNom(){
		return $this->per_nom;
	}
	public function setPerNom($num){
		$this->per_nom=$num;
	}


	public function getPerPrenom(){
		return $this->per_prenom;
	}
	public function setPerPrenom($prenom){
		$this->per_prenom=$prenom;
	}


	public function getPerTel(){
		return $this->per_tel;
	}
	public function setPerTel($tel){
		$this->per_tel=$tel;
	}


	public function getPerMail(){
		return $this->per_mail;
	}
	public function setPerMail($mail){
		$this->per_mail=$mail;
	}


	public function getPerAdmin(){
		return $this->per_admin;
	}
	public function setPerAdmin($adm){
		$this->per_admin=$adm;
	}


	public function getPerLogin(){
		return $this->per_login;
	}
	public function setPerLogin($log){
		$this->per_login=$log;
	}


	public function getPerPwd(){
		return $this->per_pwd;
	}
	public function setPerPwd($pwd){
		$this->per_pwd=$pwd;
	}

}
