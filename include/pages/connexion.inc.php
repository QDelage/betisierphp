<h1>Pour vous connecter</h1>

<?php
$pdo=new Mypdo();
if (empty($_POST["pseudo"]) && empty($_POST["pseudo"]) || $_POST["resu"] != $_SESSION["resu"]) {
?>


<form method="post" action="index.php?page=14">

    <label>Nom d'utilisateur :</label> <input required name="pseudo" type="text">
    <br /> <br />

    <label>Mot de passe :</label> <input required name="pass" type="password">

    <br /><br />
    <!-- Pour un nombre aléatoire -->
    <?php $nb1=rand(1,9);
    $nb2=rand(1,9);
    $_SESSION["resu"] = $nb1 + $nb2;?>

    <img src="image/nb/<?php echo $nb1 ?>.jpg"> + <img src="image/nb/<?php echo $nb2 ?>.jpg"> = <input required type="text" name="resu">

    <br /><br />
    <?php
    if (!empty($_POST["resu"])) {
        ?>
        <label>Erreur de validation du captcha, vérifiez le calcul</label>
        <?php
    }
     ?>
    <br /> <br />
    <input type="submit" value="Valider"></input>

</form>

<?php } else {
    $cm = new ConnexionManager($pdo);

    if ($cm->verifConnexion($_POST["pseudo"], $_POST["pass"])) {
        ?>
        <label>Vous avez bien été connecté !</label>
        <p>Redirection automatique dans 1 secondes<p>

        <?php
        $pm = new PersonneManager($pdo);

        // Pour les affichages etc, on garde s'il est etudiant ou pas
        if ($pm->isAdmin($pm->getIdByPseudo($_POST["pseudo"]))) {
            $_SESSION['admin'] = true;
        }

        if ($pm->isEtudiant($pm->getIdByPseudo($_POST["pseudo"]))) {
            // on récupere son id selon son pseudo, puis on vérifie qu'il est étudiant
            $_SESSION['student'] = true;
        }else {
            $_SESSION['student'] = false;
        }
        $_SESSION['login'] = $_POST["pseudo"];
        $_SESSION['connecte'] = true;
        unset($_SESSION['resu']);
        header('Refresh: 1;url=index.php');
        exit();
    }else {
        echo '<label>Erreur de mot de passe</label>';
        header('Refresh: 1;url=index.php?page=14');
    }
}
