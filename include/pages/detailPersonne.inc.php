<?php
$id=$_GET["id"];

$pdo=new Mypdo();
$pm = new PersonneManager($pdo);
$pers = $pm->getPersonneById($id);

if ($pm->isEtudiant($id)) {
    $pm = new EtudiantManager($pdo);
    ?>
    <h1>Détail de l'étudiant <?php echo $pers->getPerNom(); ?></h1>

    <table>
        <tr><th>Prénom</th><th>Mail</th><th>Tel</th><th>Département</th><th>Ville</th></tr>

        <tr>
            <td><?php echo $pers->getPerPrenom(); ?></td>
            <td><?php echo $pers->getPerMail(); ?></td>
            <td><?php echo $pers->getPerTel(); ?></td>
            <td><?php echo $pm->getDepartementById($id); ?></td>
            <td><?php echo $pm->getVilleById($id); ?></td>
        </tr>

    </table>

<?php }else {
    $pm = new SalarieManager($pdo);
    ?>
    <h1>Détail du salarié <?php echo $pers->getPerNom(); ?></h1>

    <table>
        <tr><th>Prénom</th><th>Mail</th><th>Tel</th><th>Tel pro</th><th>Fonction</th></tr>

        <tr>
            <td><?php echo $pers->getPerPrenom(); ?></td>
            <td><?php echo $pers->getPerMail(); ?></td>
            <td><?php echo $pers->getPerTel(); ?></td>
            <td><?php echo $pm->getTelProById($id); ?></td>
            <td><?php echo $pm->getFonctionById($id); ?></td>
        </tr>

    </table>

    <?php
}

?>
