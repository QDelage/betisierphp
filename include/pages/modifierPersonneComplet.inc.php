<?php
$id=$_GET["id"];

$pdo=new Mypdo();
$pm = new PersonneManager($pdo);
$pers = $pm->getPersonneById($id);
$sm = new SalarieManager($pdo);
$em = new EtudiantManager($pdo);


if (empty($_POST['nom']) && $pm->isEtudiant($id)) {
    ?>
    <h1>Modification de l'étudiant <?php echo $pers->getPerNom(); ?></h1>

    <form action="" method="post">

        <label>Nom : </label><input name="nom" type="text" value="<?php echo $pers->getPerNom(); ?>"><br /><br />
        <label>Prénom : </label><input name="pre" type="text" value="<?php echo $pers->getPerPrenom(); ?>"><br /><br />
        <label>Mail : </label><input name="mail" type="mail" value="<?php echo $pers->getPerMail(); ?>"><br /><br />
        <label>Login : </label><input name="login" type="text" value="<?php echo $pers->getPerLogin(); ?>"><br /><br />
        <label>Mot de passe : </label><input name="pwd" type="password" value="******"><br /><br />
        <label>Téléphone : </label><input name="tel" type="tel" value="<?php echo $pers->getPerTel(); ?>"><br /><br />
        <label>Année : </label><select name="div">
            <?php

                $anneeM = new DivisionManager($pdo);
                $annees = $anneeM->getAllDivisions();

                foreach ($annees as $key => $value) { ?>

                    <option value="<?php echo $value->div_num ?>"<?php if ($value->div_num == $em->getDivNumById($id)) {
                       echo " selected";
                   } ?>><?php echo $value->div_nom ?> </option>
                <?php }
             ?>
        </select><br /><br />

        <label>Département : </label><select name="dep">
            <?php
                $depMn = new DepartementManager($pdo);
                $deps = $depMn->getAllDepartements();

                foreach ($deps as $key => $value) {?>
                    <option value="<?php echo $value->getDepNum() ?>"<?php if ($value->getDepNum() == $em->getDepNumById($id)) {
                       echo " selected";
                   } ?>><?php echo $value->getDepNom()." (".$value->getVilNom().")" ?> </option>
            <?php } ?>
        </select><br />


        <br /><br />
        <input type="submit" value="Valider les changements"><br /><br />
    </form>

<?php }else if (empty($_POST['nom']) && !empty($_GET['id'])){
    ?>
    <h1>Modification du salarié <?php echo $pers->getPerNom(); ?></h1>

    <form action="" method="post">

        <label>Nom : </label><input name="nom" type="text" value="<?php echo $pers->getPerNom(); ?>"><br /><br />
        <label>Prénom : </label><input name="pre" type="text" value="<?php echo $pers->getPerPrenom(); ?>"><br /><br />
        <label>Mail : </label><input name="mail" type="mail" value="<?php echo $pers->getPerMail(); ?>"><br /><br />
        <label>Login : </label><input name="login" type="text" value="<?php echo $pers->getPerLogin(); ?>"><br /><br />
        <label>Mot de passe : </label><input name="pwd" type="password" value="******"><br /><br />
        <label>Téléphone pro : </label><input name="telPro" type="tel" value="<?php echo $sm->getTelProById($id); ?>"><br /><br />
        <label>Téléphone : </label><input name="tel" type="tel" value="<?php echo $pers->getPerTel(); ?>"><br /><br />
        <label>Fonction : </label><select name="fct">
            <?php

                $fctManager = new FonctionManager($pdo);
                $fcts = $fctManager->getAllFonctions();

                foreach ($fcts as $key => $value) { ?>
                    <option value="<?php echo $value->getFonNum() ?>"<?php
                             if ($value->getFonNum() == $sm->getFonctionNumById($id)) {
                                echo " selected";
                            }
                        ?>><?php echo $value->getFonLibelle() ?> </option>
                <?php }  ?>
            </select><br />


        <br /><br />
        <input type="submit" value="Valider les changements"><br /><br />
    </form>


<?php } else {?>


    <?php if (!empty($_POST['telPro'])) {
            $sm->updateSalarie($id, $_POST['nom'], $_POST['pre'], $_POST['tel'], $_POST['mail'], $_POST['login'], $_POST['pwd'], $_POST['fct'], $_POST['telPro']);
            ?><h1>Mise à jour du salarié réussie</h1> <?php
        } else {
            $em->updateEtudiant($id, $_POST['nom'], $_POST['pre'], $_POST['tel'], $_POST['mail'], $_POST['login'], $_POST['pwd'], $_POST['div'], $_POST['dep']);
            ?><h1>Mise à jour de l'étudiant réussie</h1> <?php
        }?>
        
        <form action="index.php" method="post">
            <input type="submit" value="OK">
        </form>


<?php } ?>
