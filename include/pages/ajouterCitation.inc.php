<h1>Ajouter une citation</h1>

<?php  $pdo = new Mypdo();
$mm = new MotManager($pdo);

if (empty($_POST['citation']) || (!empty($_POST['citation']) && $mm->analyseStr($_POST['citation']))) { ?>
<form action="" method="post">
    <label>Enseignant : </label>
    <select required name="prof">
        <option value="" selected>Veuillez choisir</option>
        <?php
            $sm = new SalarieManager($pdo);
            $profs = $sm->getAllEnseignants();

            foreach ($profs as $key => $value) {?>
                <?php print_r($value); ?>
                <option value="<?php echo $value->getPerNum() ?>"><?php echo $value->getPerNom() ?> </option>
        <?php } ?>
    </select>
    <br /><br />
    <label>Date Citation</label>
    <input type="date" name="date" required>
    <br /><br />
    <label>Citation</label>
    <textarea required name="citation" rows="5" cols="30"><?php
        if (!empty($_POST['citation']) && $mm->analyseStr($_POST['citation'])) {
            $nouv = strToLower($_POST['citation']);
            foreach ($mm->analyseStr($_POST['citation']) as $key => $value) {
                // On sépare en différentes parties par les mots interdits
                // En lowerCase pour éviter les erreurs de maj
                $explode = explode($value->mot_interdit, $nouv);
                $nouv = "";

                // Pour chaque partie séparée des mots interdits, on les assemble avec '---'
                foreach ($explode as $key => $value) {
                    $nouv .= $value.'---';
                }

                // On a rajouté une fois de trop les '---', donc on les supprime
                $nouv = substr($nouv, 0, strlen($nouv) - 3);
            }
            echo $nouv;

        }
    ?></textarea><br /><br />

    <?php

    if (!empty($_POST['citation']) && $mm->analyseStr($_POST['citation'])) {
        foreach ($mm->analyseStr($_POST['citation']) as $key => $value) {
            echo '<img src="image/erreur.png" alt="erreur"> ';
            echo "<label>Le mot <span class=\"red\">".$value->mot_interdit."</span> n'est pas autorisé</label><br \>";
        }
     } ?>

    <br /><br />
    <input type="submit" value="Valider">
</form>

<?php } else { ?>

    <?php
    $cm = new CitationManager($pdo);
    $pm = new PersonneManager($pdo);
    $id = $pm->getIdByPseudo($_SESSION['login']);
    $cm->add($_POST['prof'], $id, $_POST['citation'], $_POST['date']);
    ?>




    <h2>Citation ajoutée avec succès</h2>
<?php } ?>
