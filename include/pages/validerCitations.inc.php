<h1>Valider les citations</h1>

<?php  $pdo = new Mypdo();
$mm = new MotManager($pdo);

?>

<?php

    $pdo=new Mypdo();
    $citationManager = new CitationManager($pdo);
    $citations = $citationManager->getAllCitationsEnAttente();
    $nb = count($citations);

    $pm = new PersonneManager($pdo);
    $vm = new VoteManager($pdo);

    ?>
    <div class="sstitre"><h2>Actuellement <?php echo $nb?> citation.s est.sont en attente de validation</h2></div>

    <?php if ($nb != 0) {?>

    <table class="tab">
        <tr><th>Nom de l'enseignant</th><th>Libellé</th><th>Date</th><th>Valider citation</th></tr>
        <?php
        foreach ($citations as $citation){ ?>

            <tr><td><?php echo $pm->getNomByID($citation->getPerNum());?>
            </td><td><?php echo $citation->getCitLibelle();?>
            </td><td><?php echo $citation->getCitDate();?>
            </td><td><a href="index.php?page=11&cit=<?php
                echo $citation->getCitNum();
                ?>"><img src="image/valid.png"> </a>
            </td>
            </tr>

            <?php } ?>

        </table>
        <br />

        <?php
        if (!empty($_GET['cit'])) {
            $cm = new CitationManager($pdo);
            $id = $pm->getIdByPseudo($_SESSION['login']);

            $cm->validerCitation($_GET['cit'], $id);

            echo '<label>La citation a bien été validée</label><br /><br />';
         }
     } ?>
