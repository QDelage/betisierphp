<h1>Rechercher une citation</h1>
<?php
$pdo=new Mypdo();
$cm = new CitationManager($pdo);

?>

<!-- nom, date, note -->
<?php if (empty($_POST['prof']) && empty($_POST['note']) && empty($_POST['date'])) { ?>

    <form target="" method="post">

        <label>Sélectionnez le nom de l'enseignent : </label><br /><br />
        <select name="prof">
            <option selected value="null">Sélectionnez</option>
            <?php
                $sm = new SalarieManager($pdo);
                $profs = $sm->getAllEnseignants();

                foreach ($profs as $key => $value) {?>
                    <?php print_r($value); ?>
                    <option value="<?php echo $value->getPerNum() ?>"><?php echo $value->getPerNom() ?> </option>
            <?php } ?>
        </select><br /><br />



        <label>Sélectionnez la date :</label><br /><br />
        <input type="date" name="date"><br /><br />

        <label>Sélectionnez la note :</label><br /><br />
        <input type="number" step="0.1" min="0" max="20" name="note">



        <br /><br />
        <input type="submit" value="Rechercher">
        <input type="reset" value="Reset">


    </form>
	<br />

<?php } else { ?>
    <?php $citations = $cm->rechercherCitation($_POST['prof'], $_POST['date'], $_POST['note']);
    $nb = count($citations);
    $pm = new PersonneManager($pdo);
    $vm = new VoteManager($pdo);
    ?>
    <div class="sstitre"><h2><?php echo $nb?> citation.s correspond.ent</h2></div>

    <?php if ($nb != 0): ?>

    <table class="tab">
        <tr><th>Nom de l'enseignant</th><th>Libellé</th><th>Date</th><th>Moyenne des notes</th></tr>
        <?php
        foreach ($citations as $citation){ ?>

            <tr><td><?php echo $pm->getNomByID($citation->getPerNum());?>
            </td><td><?php echo $citation->getCitLibelle();?>
            </td><td><?php echo $citation->getCitDate();?>
            </td><td><?php echo $vm->getMoyenneByCitation($citation->getCitNum());?>
            </td>

            </tr>

            <?php } ?>

        </table>
    <?php endif; ?>

        <br />

<?php } ?>
