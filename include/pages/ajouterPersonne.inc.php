<h1>Ajouter une personne</h1>
<?php $pdo = new Mypdo();
if (empty($_POST['nom']) && empty($_SESSION["etu"]) && empty($_SESSION['sal'])) { ?>
<form action="" method="post">
    <label>Nom : </label>
    <input type="text" name="nom" required>
	<br /><br />

	<label>Prénom : </label>
    <input type="text" name="prenom" required>
	<br /><br />

	<label>Téléphone : </label>
    <input type="tel" name="tel" required>
	<br /><br />

	<label>Mail : </label>
    <input type="mail" name="mail" required>
	<br /><br />

	<label>Login : </label>
    <input type="text" name="login" required>
	<br /><br />

	<label>Mot de passe : </label>
    <input type="password" name="pass" required>
	<br /><br />

	<label>Catégorie : </label>

	<input type="radio" name="cat" value="etu" required>
    <span>Étudiant</span>

	<input type="radio" name="cat" value="sal" required>
    <span>Salarié</span>
	<br /><br />



    <br /><br />
    <input type="submit" value="Valider">
</form>
<?php
} else if (!empty($_POST['cat'])) { ?>
    <?php


    if (empty($_SESSION["etu"]) && $_POST['cat'] == "etu") {
        // On crée une personne et on lui affecte ses valeurs
        $etu = new Etudiant();
        $etu->setPerNom($_POST['nom']);
        $etu->setPerPrenom($_POST['prenom']);
        $etu->setPerTel($_POST['tel']);
        $etu->setPerMail($_POST['mail']);
        $etu->setPerLogin($_POST['login']);
        $etu->setPerPwd($_POST['pass']);

        // Pour stocker un objet, il faut le serializer.
        $_SESSION["etu"] = serialize($etu);


         ?>
        <form action="" method="post">
            <label>Année : </label>
            <select name="div">
                <?php

                    $anneeM = new DivisionManager($pdo);
                    $annees = $anneeM->getAllDivisions();

                    foreach ($annees as $key => $value) { ?>

                        <option value="<?php echo $value->div_num ?>"><?php echo $value->div_nom ?> </option>
                    <?php }
                 ?>
            </select>

            <br /><br />
            <label>Département : </label>
            <select name="dep">
                <?php
                    $depMn = new DepartementManager($pdo);
                    $deps = $depMn->getAllDepartements();

                    foreach ($deps as $key => $value) {?>
                        <option value="<?php echo $value->getDepNum() ?>"><?php echo $value->getDepNom()." (".$value->getVilNom().")" ?> </option>
                <?php } ?>
            </select>
        	<br /><br />

            <br /><br />
            <input type="submit" value="Valider">
        </form>

    <?php } else if (empty($_SESSION['sal']) && $_POST['cat'] == 'sal'){ ?>
        <?php
        // On crée une personne et on lui affecte ses valeurs
        $sal = new Salarie();
        $sal->setPerNom($_POST['nom']);
        $sal->setPerPrenom($_POST['prenom']);
        $sal->setPerTel($_POST['tel']);
        $sal->setPerMail($_POST['mail']);
        $sal->setPerLogin($_POST['login']);
        $sal->setPerPwd($_POST['pass']);

        // Pour stocker un objet, il faut le serializer.
        $_SESSION['sal'] = serialize($sal);



        ?>

        <form action="" method="post">
            <label>Téléphone professionnel : </label>
            <input name="tel" type="tel">
            <br /><br />
            <label>Fonction</label>
            <select name="fct">
                <?php

                    $fctManager = new FonctionManager($pdo);
                    $fcts = $fctManager->getAllFonctions();

                    foreach ($fcts as $key => $value) { ?>
                        <option value="<?php echo $value->getFonNum() ?>"><?php echo $value->getFonLibelle() ?> </option>
                    <?php }  ?>
            </select>


            <br /><br />
            <input type="submit" value="Valider">
        </form>
    <?php } ?>

<?php } else if (!empty($_SESSION["etu"]))  { ?>
    <?php
    // Enfin, on peut le désérializer.

    $etu = unserialize($_SESSION["etu"]);
    $etu->setDivNum($_POST["div"]);
    $etu->setDepNum($_POST["dep"]);

    $em = new EtudiantManager($pdo);
    $em->add($etu);

    echo '<img src="image/valid.png" alt="valide">'." L'étudiant à bien été ajouté";

    unset($_SESSION['etu']);


     ?>

<?php }else if (!empty($_SESSION['sal']))   {

    // Enfin, on peut le désérializer.

    $sal = unserialize($_SESSION["sal"]);
    $sal->setTelProf($_POST["tel"]);
    $sal->setFonNum($_POST["fct"]);

    $sm = new SalarieManager($pdo);
    $sm->add($sal);

    echo '<img src="image/valid.png" alt="valide">'." Le salarié à bien été ajouté";

    unset($_SESSION['sal']);




 } ?>
