
	<h1>Liste des citations déposées</h1>
	<?php $pdo = new Mypdo();?>
	<?php
		$pdo=new Mypdo();
		$citationManager = new CitationManager($pdo);
		$citations = $citationManager->getAllCitationsValides();
		$nb = count($citations);

		$pm = new PersonneManager($pdo);
		$vm = new VoteManager($pdo);

		?>
		<div class="sstitre"><h2>Actuellement <?php echo $nb?> citations sont enregistrés</h2></div>



		<table class="tab">
			<tr><th>Nom de l'enseignant</th><th>Libellé</th><th>Date</th><th>Moyenne des notes</th></tr>
			<?php
			foreach ($citations as $citation){ ?>

				<tr><td><?php echo $pm->getNomByID($citation->getPerNum());?>
				</td><td><?php echo $citation->getCitLibelle();?>
				</td><td><?php echo $citation->getCitDate();?>
				</td><td><?php echo $vm->getMoyenneByCitation($citation->getCitNum());?>
				</td>
					<?php if (!empty($_GET['cit']) && $_GET['cit'] == $citation->getCitNum() ) { ?>
						<td>
						<form action="" method="post">
							<input name="note" type="number" min="0" max="20">
							<input type="submit" value="OK">
						</form>
						</td>
					<?php } else if (!empty($_SESSION['student']) && $_SESSION['student']) { ?>
						<td><a href="index.php?page=6&cit=<?php echo $citation->getCitNum(); ?>"><img src="image/modifier.png"> </a></td>
					<?php } ?>

				</tr>

				<?php } ?>

			</table>
			<br />

			<?php
			if (isset($_POST['note'])) {
				$pm = new PersonneManager($pdo);
				$id = $pm->getIdByPseudo($_SESSION['login']);
				$vm = new VoteManager($pdo);
				if ($vm->noterCitation($_POST['note'], $id,$_GET['cit']) == 1){
					// Pour que la nouvelle moyenne s'affiche
					header('Refresh: 0;url=index.php?page=6&fait=true');
				}else{
					echo "<label>Vous ne pouvez pas noter deux fois la même citation</label><br /><br />";
				}
			 }
			 if (!empty($_GET['fait'])) {
				 echo "<label>Note ajoutée</label><br /><br />";
			 }
			 ?>
