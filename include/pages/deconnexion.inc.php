<h1>Déconnexion</h1>

<?php

if (!empty($_SESSION['connecte'])) {
    $_SESSION['connecte'] = false;
    unset($_SESSION['login']);
    session_unset();
}

?>
<label>Vous avez bien été déconnecté !</label>
<p>Redirection automatique dans 1 secondes<p>

<?php
header('Refresh: 1;url=index.php');
exit();


?>
