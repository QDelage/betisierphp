<h1>Supprimer Ville</h1>
<?php
$pdo=new Mypdo();
$villeManager = new VilleManager($pdo);
$villes = $villeManager->getAllVilles();
?>

<?php if (empty($_POST['ville'])) { ?>

    <form target="" method="post">

        <label>Sélectionnez la ville à supprimer :</label>
        <br /><br />
        <select name="ville">
            <?php foreach ($villes as $key => $value) { ?>
                <?php if (!$villeManager->aDesDepartements($value->getVilNum())){ ?>
                    <option value="<?php echo $value->getVilNum(); ?>"><?php echo $value->getVilNom(); ?></option>
                <?php } ?>
            <?php } ?>
        </select>

        <br /><br />

        <input type="submit" value="Supprimer">


    </form>

<?php } else { ?>
    <?php $villeManager->supprimerVille($_POST['ville']); ?>
    <h2>Ville supprimée<h2>
<?php } ?>
