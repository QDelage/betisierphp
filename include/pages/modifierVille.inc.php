<h1>Modifier une ville</h1>
<?php
$pdo=new Mypdo();
$vm = new VilleManager($pdo);
$villes = $vm->getAllVilles();

?>


<?php if (empty($_POST['ville'])) { ?>

    <form target="" method="post">

        <label>Sélectionnez la ville à modifier :</label>
        <br /><br />
        <select name="ville">
            <?php foreach ($villes as $key => $value) { ?>
                <option value="<?php echo $value->getVilNum(); ?>"><?php echo $value->getVilNom(); ?></option>
            <?php } ?>
        </select><br /><br />

        <label>Indiquez son nouveau nom :</label><br /><br />
        <input type="text" required name="nom">



        <br /><br />

        <input type="submit" value="Modifier">


    </form>
	<br />

<?php } else { ?>
    <?php $vm->modifierVille($_POST['ville'], $_POST['nom']); ?>
    <h2>Ville modifiée<h2>
<?php } ?>
