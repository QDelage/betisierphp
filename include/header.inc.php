<?php session_start(); ?>
<!doctype html>
<html lang="fr">
<head>
  <meta charset="utf-8">
  <?php
		$title = "Bienvenue sur le site du bétisier de l'IUT.";?>
		<title>
		<?php echo $title ?>
		</title>

		<link rel="stylesheet" type="text/css" href="css/stylesheet.css" />

</head>
	<body>
	<div id="header">
		<div id="connect">
            <?php
            if (empty($_SESSION['connecte']) || !$_SESSION['connecte']) {
                ?>
                <a href="index.php?page=14">Connexion</a>
            <?php }else{ ?>
                <span>Utilisateur : <?php echo $_SESSION['login'] ?> </span><a href="index.php?page=15">Déconnexion</a>
            <?php } ?>

		</div>
		<div id="entete">
			<div id="logo">
                <?php if (!empty($_SESSION['connecte']) and $_SESSION['connecte']) { ?>
                    <img src="image/smile.jpg" alt="LogoConnecte">
                <?php }else { ?>
                 <img src="image/lebetisier.gif" alt="LogoDeconnecte">
             <?php } ?>
			</div>
			<div id="titre">

				Le bétisier de l'IUT,<br />Partagez les meilleures perles !!!
			</div>
		</div>
	</div>
