<div id="texte">
<?php
if (!empty($_GET["page"])){
	$page=$_GET["page"];}
	else
	{$page=0;
	}

 if (!empty($_SESSION['connecte']) && $_SESSION['connecte']) {
	$connecte = true;
		if(!empty($_SESSION['admin']) && $_SESSION['admin']) {
			$admin = true;
		}else {
			$admin = false;
		}

 }else {
 	$connecte = false;
 }

// Les pages accessibles dépendent du statut de la personne
 if ($connecte) {
	 // Si c'est un admin, il a d'autres accès en plus de ceux de base
	 if ($admin) {

			 if (!empty($_SESSION['student']) && $_SESSION['student']) {
	 			switch ($page) {
				// Admin :
				case 3:
				   include("pages/ModifierPersonne.inc.php");
					break;

				case 4:
				   include_once('pages/supprimerPersonne.inc.php');
					break;

				case 9:
				   include("pages/supprimerVille.inc.php");
					break;

				case 10:
				   include("pages/modifierPersonneComplet.inc.php");
					break;

				case 11:
				   include("pages/validerCitations.inc.php");
					break;

				case 12:
				   include("pages/supprimerCitation.inc.php");
					break;

				case 16:
				   include("pages/modifierVille.inc.php");
				   break;

				// Étudiant

	 			case 0:
	 				include_once('pages/accueil.inc.php');
	 				break;

	 			case 2:
	 				include_once('pages/listerPersonnes.inc.php');
	 			    break;

	 			case 5:
	 			    include("pages/ajouterCitation.inc.php");
	 			    break;

	 			case 6:
	 				include("pages/listerCitation.inc.php");
	 			    break;

	 			case 7:
	 				include("pages/ajouterVille.inc.php");
	 			    break;

	 			case 8:
	 				include("pages/listerVilles.inc.php");
	 			    break;

	 			case 13:
	 				include("pages/detailPersonne.inc.php");
	 				break;

	 			case 15:
	 				include("pages/deconnexion.inc.php");
	 				break;

	 			case 16:
	 				include("pages/modifierVille.inc.php");
	 				break;

	 			case 17:
	 				include("pages/rechercherCitation.inc.php");
	 				break;

	 			default :
	 				include_once('pages/pageIntrouvable.inc.php');

	 			}
	 		// Si c'est un prof
	 	 	} else {
	 			switch ($page) {

				// Admin :
				case 3:
				   include("pages/ModifierPersonne.inc.php");
					break;

				case 4:
				   include_once('pages/supprimerPersonne.inc.php');
					break;

				case 9:
				   include("pages/supprimerVille.inc.php");
					break;

				case 10:
				   include("pages/modifierPersonneComplet.inc.php");
					break;

				case 11:
				   include("pages/validerCitations.inc.php");
					break;

				case 12:
				   include("pages/supprimerCitation.inc.php");
					break;

				case 16:
				   include("pages/modifierVille.inc.php");
				   break;

				// Salarie

	 			case 0:
	 				include_once('pages/accueil.inc.php');
	 				break;

	 			case 1:
	 				include("pages/ajouterPersonne.inc.php");
	 			    break;

	 			case 2:
	 				include_once('pages/listerPersonnes.inc.php');
	 			    break;;

	 			case 4:
	 				include_once('pages/supprimerPersonne.inc.php');
	 			    break;

	 			case 6:
	 				include("pages/listerCitation.inc.php");
	 			    break;

	 			case 7:
	 				include("pages/ajouterVille.inc.php");
	 			    break;

	 			case 8:
	 				include("pages/listerVilles.inc.php");
	 			    break;

	 			case 13:
	 				include("pages/detailPersonne.inc.php");
	 				break;

	 			case 15:
	 				include("pages/deconnexion.inc.php");
	 				break;

	 			case 17:
	 				include("pages/rechercherCitation.inc.php");
	 				break;

	 			default :
	 			include_once('pages/pageIntrouvable.inc.php');

	 			}
	 	 	}

	 }else if (!empty($_SESSION['student']) && $_SESSION['student']) {
		// Si c'est un étudiant

		switch ($page) {
		case 0:
			include_once('pages/accueil.inc.php');
			break;

		case 2:
			include_once('pages/listerPersonnes.inc.php');
		    break;

		case 5:
		    include("pages/ajouterCitation.inc.php");
		    break;

		case 6:
			include("pages/listerCitation.inc.php");
		    break;

		case 7:
			include("pages/ajouterVille.inc.php");
		    break;

		case 8:
			include("pages/listerVilles.inc.php");
		    break;

		case 13:
			include("pages/detailPersonne.inc.php");
			break;

		case 15:
			include("pages/deconnexion.inc.php");
			break;

		case 16:
			include("pages/modifierVille.inc.php");
			break;

		case 17:
			include("pages/rechercherCitation.inc.php");
			break;

		default :
			include_once('pages/pageIntrouvable.inc.php');

		}
	// Si c'est un prof
}else if (!empty($_SESSION['student']) && !$_SESSION['student']) {
		switch ($page) {
		case 0:
			include_once('pages/accueil.inc.php');
			break;

		case 1:
			include("pages/ajouterPersonne.inc.php");
		    break;

		case 2:
			include_once('pages/listerPersonnes.inc.php');
		    break;;

		case 4:
			include_once('pages/supprimerPersonne.inc.php');
		    break;

		case 6:
			include("pages/listerCitation.inc.php");
		    break;

		case 7:
			include("pages/ajouterVille.inc.php");
		    break;

		case 8:
			include("pages/listerVilles.inc.php");
		    break;

		case 13:
			include("pages/detailPersonne.inc.php");
			break;

		case 15:
			include("pages/deconnexion.inc.php");
			break;

		case 17:
			include("pages/rechercherCitation.inc.php");
			break;

		default:
		include_once('pages/pageIntrouvable.inc.php');

		}
 	}
	// S'il n'est pas connecté, il n'est ni salarié, ni étudiant, ni admin
}else {

	switch ($page) {
	case 0:
		include_once('pages/accueil.inc.php');
		break;

	case 2:
		include_once('pages/listerPersonnes.inc.php');
	    break;

	case 6:
		include("pages/listerCitation.inc.php");
	    break;

	case 8:
		include("pages/listerVilles.inc.php");
	    break;

	case 13:
		include("pages/detailPersonne.inc.php");
		break;

	case 14:
		include("pages/connexion.inc.php");
		break;

	default :
		include_once('pages/pageIntrouvable.inc.php');

	}
}



?>
</div>
